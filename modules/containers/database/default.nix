#
# Sql Server container
#

{ config, lib, pkgs, ... }:

{
  virtualisation.oci-containers.backend = "podman";
  virtualisation.oci-containers.containers = {
    sql_server1 = {
      image = "mcr.microsoft.com/mssql/server:2022-latest";
      autoStart = true;
      ports = [ "127.0.0.1:1433:1433" ];
      environment = {
        ACCEPT_EULA = "Y";
        MSSQL_SA_PASSWORD ="#135SqlserverAcess%";
      };
      extraOptions = [ "-h=sql_server1" "--detach" ];
    };
  };
}
