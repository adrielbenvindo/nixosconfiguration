#
# Social
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    pkgs.libsForQt5.kaddressbook
  ];
}
