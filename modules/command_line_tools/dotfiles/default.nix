#
# Dotfiles Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      stow
    ];
  };
}
