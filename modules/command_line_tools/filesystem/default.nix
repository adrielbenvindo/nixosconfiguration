#
# Filesystem Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      bat
      exa
      ripgrep
      sd
      fd
      du-dust
      tokei
      zoxide
      delta
      ranger
    ];
  };
}
