#
# Process Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      htop
      procs
      du-dust
      hyperfine
      bandwhich
      delta
    ];
  };
}
