#
# Files Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      tokei
      delta
      viu
    ];
  };
}
