{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./documentation)] ++
    [(import ./dotfiles)] ++
    [(import ./environment)] ++
    [(import ./file)] ++
    [(import ./filesystem)] ++
    [(import ./integration)] ++
    [(import ./process)] ++
    [(import ./regex)] ++
    [(import ./web_acess)] ++
    [(import ./ftp)] ++
    [(import ./user_acess)];
}
