#
# Web Acess Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      wget
      curlFull
      inetutils
    ];
  };
}
