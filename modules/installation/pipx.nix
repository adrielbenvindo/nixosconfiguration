#
# Python pipx installation
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    python39Packages.pipx
  ];
}
