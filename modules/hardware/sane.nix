#
# sane
#

{ pkgs, lib, user, ... }:

{
  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.sane-airscan ];
  };
}
