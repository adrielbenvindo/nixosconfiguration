
#
# Git
#

{ pkgs, lib, ...}:

{
  programs = {
    git = {
      enable = true;
      package = pkgs.gitAndTools.gitFull;
    };
  };
}
