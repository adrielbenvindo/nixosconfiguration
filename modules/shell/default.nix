{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./git.nix)] ++
    [(import ./zsh.nix)] ++
    [(import ./ksh.nix)] ++
    [(import ./starship.nix)] ++
    [(import ./powershell.nix)] ++
    [(import ./nushell.nix)] ++
    [(import ./direnv.nix)] ++
    [(import ./tmux.nix)];
}
