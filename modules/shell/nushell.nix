#
# NuShell
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      nushell
    ];
  };
}
