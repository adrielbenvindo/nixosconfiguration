#
# KSH Shell
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      ksh
    ];
  };
}
