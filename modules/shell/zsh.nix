#
# ZSH Shell
#

{ pkgs, ... }:

{
  # programs = {
    # zsh = {
      # enableBashCompletion = true;
      # zsh-autoenv.enable = true;
      # syntaxHighlighting.enable = true;
      # autosuggestions.enable = true;
                
      # shellAliases = {
        # ll = "ls -l";
        # update = "sudo nixos-rebuild switch";
      # };
      # shellInit = "export ANDROID_HOME=$HOME/Android/Sdk \n export PATH=$PATH:$ANDROID_HOME/emulator \n export PATH=$PATH:$ANDROID_HOME/platform-tools";
      # histSize = 10000;
      # histFile = "$HOME/.zsh_history";
    # };
  # };
  environment = {
    systemPackages = with pkgs; [
      zsh
    ];
  };
}
