#
# PowerShell
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      powershell
      pash
    ];
  };
}
