#
# OS Management
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      ventoy-bin-full
    ];
  };
}
