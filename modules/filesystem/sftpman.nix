#
# sFTPMAN
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      sftpman
    ];
  };
}
