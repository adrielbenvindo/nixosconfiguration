{ config, pkgs, lib, user, ... }:

{
  imports = 
    [(import ./sshfs.nix)] ++
    [(import ./sftpman.nix)];
}
