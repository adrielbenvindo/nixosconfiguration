#
# SSHFS
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      sshfs
    ];
  };
}
