#
# MSSQL Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 3306 ];
  networking.firewall.allowedUDPPorts = [ 3306 ];
}
