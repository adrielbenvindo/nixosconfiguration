#
# Database Ports
#

{ config, lib, pkgs, ... }:

{
  imports =
    [(./mssql.nix)] ++
    [(./mysql.nix)];
}
