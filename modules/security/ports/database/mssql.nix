#
# MSSQL Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 1433 ];
  networking.firewall.allowedUDPPorts = [ 1433 ];
}
