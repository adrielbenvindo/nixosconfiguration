#
# Go Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 4000 ];
  networking.firewall.allowedUDPPorts = [ 4000 ];
}
