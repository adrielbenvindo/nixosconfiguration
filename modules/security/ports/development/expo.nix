#
# Expo Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 19000 ];
  networking.firewall.allowedUDPPorts = [ 19000 ];
}
