#
# Java Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 8080 ];
  networking.firewall.allowedUDPPorts = [ 8080 ];
}
