#
# Dotnet Ports
#

{ config, lib, pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 5252 ];
  networking.firewall.allowedUDPPorts = [ 5252 ];
}
