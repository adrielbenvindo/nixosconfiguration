#
# Backup
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      tarsnapper
      snapper
      snapper-gui
      #timeshift
    ];
  };
}
