{ config, pkgs, lib, user, ... }:

{
  imports = 
    [(import ./gnupg.nix)] ++
    [(import ./backup)] ++
    [(import ./ports)];
}
