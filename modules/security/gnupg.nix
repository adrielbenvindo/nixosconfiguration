#
# GnuPG
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      gnupg
      #pinentry-curses
    ];
  };
  #services.pcscd.enable = true;
  #programs.gnupg.agent = {
   #enable = true;
   #pinentryFlavor = "curses";
   #enableSSHSupport = true;
  #};
}
