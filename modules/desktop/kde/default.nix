#
# KDE Plasma 5 configuration
#

{ config, lib, pkgs, ... }:


{
  #nixpkgs.overlays = [
    #(self: super: {
      #plasma-desktop = pkgs.unstable.plasma-desktop;
    #})
  #];
  programs = {
    dconf.enable = true;
    kdeconnect = {                                # For GSConnect
      enable = true;
      #package = pkgs.libsForQt5.kdeconnect-kde;
    };
  };
  
  services = {
    xserver = {
      enable = true;

      #layout = "us";                              # Keyboard layout & €-sign
      #xkbOptions = "eurosign:e";
      libinput.enable = true;
      #modules = [ pkgs.xf86_input_wacom ];        # Both needed for wacom tablet usage
      #wacom.enable = true;

      displayManager = {
        sddm.enable = true;          # Display Manager
        defaultSession = "plasmawayland";
      };
      desktopManager.plasma5 = {
        enable = true;                            # Desktop Manager
        excludePackages = with pkgs.libsForQt5; [
          oxygen
        ];
      };
    };
  };
  
  environment = {
    systemPackages = with pkgs.libsForQt5; [                 # Packages installed
      packagekit-qt
      falkon
      konqueror
      knotes
      yakuake
      kcalc
      kfind
      discover
      kalendar
      ktorrent
      kget
      pkgs.libsForQt5.filelight
      pkgs.partition-manager
      kmag
      pkgs.digikam
      kmix
      krfb
      krdc
      kamoso
      kgpg
      pkgs.bleachbit
      ark
    ];
    #plasma5.excludePackages = with pkgs.libsForQt5; [
      #oxygen
    #];
  };
}
