{ pkgs, lib, user, ... }:

{
  imports = 
   [(import ./dropbox.nix)] ++ 
   [(import ./onedrive.nix)] ++
   [(import ./pcloud.nix)];
}
