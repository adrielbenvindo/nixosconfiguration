#
# Distrobox
#

{ config, pkgs, user, ... }:

{
  environment.systemPackages = with pkgs; [
    distrobox
  ];
}
