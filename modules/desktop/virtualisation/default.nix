{ pkgs, lib, pkgs-stable, lib-stable, nixpkgs-stable, user, ... }:

{
  imports = 
    [(import ./docker.nix)] ++
    [(import ./qemu.nix)] ++
    [(import ./podman.nix)] ++
    [(import ./distrobox.nix)];
}
