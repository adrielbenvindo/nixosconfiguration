
#
# Podman
#

{ config, pkgs, lib, user, ... }:#nixpkgs-unstable

{
  #imports = [
    #"${nixpkgs-unstable}/pkgs/applications/virtualization/podman/default.nix"
  #];

  #disabledModules = [
    #"pkgs/applications/virtualization/podman/default.nix"
  #];
  virtualisation = {
    podman = {
      enable = true;
      defaultNetwork.dnsname.enable = true;
      #defaultNetwork.settings = { dnsname = true; };
    };
  };

  #environment = {
  #  interactiveShellInit = ''
  #    alias rtmp='podman start nginx-rtmp'
  #  '';                                                           # Alias to easily start container
  #};

  environment.systemPackages = with pkgs; [
    podman-compose
    pods
    podman-tui
  ];
}
