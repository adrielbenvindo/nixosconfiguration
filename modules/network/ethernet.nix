#
# Ethernet Configuration
#

{ config, pkgs, lib, ... }:

{
  networking = {
    interfaces = {
      enp1s0 = {                                # Change to correct network driver
        useDHCP = true;                         # Disabled because fixed ip
      #   ipv4.addresses = [ {                    # Ip settings: *.0.50 for main machine
      #     address = "192.168.0.50";
      #     prefixLength = 24;
      #   } ];
      };
    };
  };
}
