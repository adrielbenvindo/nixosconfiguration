#
# DHCP Configuration
#

{ config, pkgs, lib, ... }:

{
  networking.useDHCP = true;
}
