#
# IPV6 Configuration
#

{ config, pkgs, lib, ... }:

{
  networking.enableIPv6 = true;
}
