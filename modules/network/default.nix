{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./network_manager.nix)] ++
    #[(import ./dhcp.nix)] ++
    [(import ./ipv6.nix)];
    #[(import ./bridges.nix)];
    #[(import ./wlan.nix)];
    #[(import ./ethernet.nix)];
    #[(import ./servers.nix)];
    #[(import ./wpa_supplicant.nix)];
}
