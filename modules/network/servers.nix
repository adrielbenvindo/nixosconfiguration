#
# Servers Configuration
#

{ config, pkgs, lib, ... }:

{
  networking = {
    defaultGateway = "192.168.0.1";
    nameservers = [ "192.168.0.4" "1.1.1.1"];   # Pi-Hole DNS
    #nameservers = [ "1.1.1.1" "1.0.0.1" ];     # Cloudflare (when Pi-Hole is down)
  };
}
