#
# Wlan Configuration
#

{ config, pkgs, lib, ... }:

{
  networking = {
    interfaces = {
      wlp2s0.useDHCP = true;                   # Wireless card
    };
  };
}
