#
# Bridges Configuration
#

{ config, pkgs, lib, ... }:

{
  networking = {
    bridges = {    # Bridge so interface can be used with virtual machines
      "br0" = {
        interfaces = [ "enp1s0" ];
      };
    };
    interfaces = {
      # enp2s0 = {                                # Change to correct network driver
      #   #useDHCP = true;                         # Disabled because fixed ip
      #   ipv4.addresses = [ {                    # Ip settings: *.0.50 for main machine
      #     address = "192.168.0.50";
      #     prefixLength = 24;
      #   } ];
      # };
      # wlp1s0.useDHCP = true;                   # Wireless card
      br0.ipv4.addresses = [{
        address = "192.168.0.50";
        prefixLength = 24;
      } ];
    };
  };
}
