#
# Vim
#
 
{ pkgs, ... }:

{
  programs = {
    vim = {
      package = pkgs.vim-full;
    };
  };
}
