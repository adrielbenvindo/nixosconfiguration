#
# Helix
#
 
{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      helix
    ];
  };
}
