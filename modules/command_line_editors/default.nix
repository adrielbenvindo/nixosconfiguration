{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./micro)] ++
    [(import ./vim)] ++
    [(import ./neovim)] ++
    [(import ./helix)];
    #[(import ./emacs/doom-emacs)];
}
