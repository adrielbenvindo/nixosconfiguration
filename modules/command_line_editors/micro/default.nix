#
# Micro
#
 
{ config, pkgs, lib, user, ... }:

{ 
  users.users.${user}.packages = with pkgs; [
    micro
  ];
}
