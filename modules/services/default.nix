{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./flatpak.nix)] ++
    [(import ./sound)] ++
    #[(import ./database)] ++
    [(import ./adb.nix)];
    #[(import ./samba.nix)] ++
    #[(import ./blueman.nix)];
}
