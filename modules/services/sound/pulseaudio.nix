#
# Pulseaudio configuration
#

{ config, pkgs, lib, ... }:

{
  hardware.pulseaudio.enable = true;
}
