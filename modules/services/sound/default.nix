#
# Flatpak
#

{ config, pkgs, lib, ... }:

{
  sound.enable = true;
  #sound.mediaKeys.enable = true;
  #imports = [(import ./pulseaudio.nix)];
  imports = [(import ./pipewire.nix)];
}
