#
# Samba service
#

{ pkgs, lib, user, ... }:

{
  services.samba = {
    enable = true;                            # Don't forget to set a password:  $ smbpasswd -a <user>
    shares = {
      share = {
        "path" = "/home/${user}";
        "guest ok" = "yes";
        "read only" = "no";
      };
    };
    openFirewall = true;
  };
}
