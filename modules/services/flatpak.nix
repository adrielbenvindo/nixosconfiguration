#
# Flatpak
#

{ config, pkgs, lib, ... }:

{
  services.flatpak.enable = true;
}
