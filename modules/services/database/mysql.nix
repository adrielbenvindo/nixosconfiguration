#
# Mysql service configuration
#

{ config, pkgs, lib, ... }:

{
  services.mysql = {
    enable = true;
    package = pkgs.mysql80; 
  };
  services.mysqlBackup = {
    enable = true;
  };
}
