#
# Adb(Android) Configuration
#

{ config, pkgs, lib, ... }:

{
  programs.adb.enable = true;
}
