#
# Graphical
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    gimp
    krita
    blender
    pkgs.libsForQt5.kolourpaint
    pkgs.libsForQt5.kcolorchooser
  ];
}
