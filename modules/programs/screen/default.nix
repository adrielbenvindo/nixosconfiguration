#
# Screen
#

{ config, lib, pkgs, user, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      simplescreenrecorder
    ];
  };
  users.users.${user}.packages = with pkgs; [
    pkgs.libsForQt5.kruler
  ];
    
}
