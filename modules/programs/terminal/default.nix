#
# Terminal Programs
#

{ pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      alacritty
    ];
  };
}
