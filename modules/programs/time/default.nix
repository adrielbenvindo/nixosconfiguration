#
# Time
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    ktimer
    kronometer
    ktimetracker
    pkgs.libsForQt5.kalarm
  ];
}
