#
# Email
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    mailspring
    thunderbird
    pkgs.libsForQt5.kmail
  ];
}
