#
# Rust Development
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    rustup
  ];
}
