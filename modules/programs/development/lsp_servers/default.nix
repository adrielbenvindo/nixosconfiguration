{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./csharp.nix)] ++
    [(import ./bash.nix)] ++
    [(import ./html.nix)] ++
    [(import ./javascript.nix)];
}
