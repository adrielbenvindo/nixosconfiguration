#
# CSharp Language server protocol
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    omnisharp-roslyn
  ];
}
