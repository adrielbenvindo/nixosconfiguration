#
# HTML Language server protocol
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    nodePackages_latest.vscode-html-languageserver-bin
  ];
}
