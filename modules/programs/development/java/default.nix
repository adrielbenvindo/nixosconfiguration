#
# Java Development
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    jdk
    jdk11
    spring-boot-cli
    maven
    gradle
    gradle-completion
  ];
}
