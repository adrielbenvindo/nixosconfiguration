#
# Database Management
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    mysql-shell
    dbeaver
  ];
}
