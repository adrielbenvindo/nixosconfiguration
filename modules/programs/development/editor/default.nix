#
# Code Editor
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    vscode
    lapce
    pkgs.libsForQt5.kate
  ];
}
