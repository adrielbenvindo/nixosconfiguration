#
# Git
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    gitkraken
    github-desktop
  ];
}
