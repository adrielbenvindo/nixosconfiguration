#
# Prototyping
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    figma-linux
  ];
}
