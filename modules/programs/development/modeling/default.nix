#
# Modeling tools
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    pkgs.libsForQt5.umbrello
    staruml
  ];
}
