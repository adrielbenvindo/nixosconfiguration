#
# Python Development
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    python39
    python39Packages.pip
  ];
}
