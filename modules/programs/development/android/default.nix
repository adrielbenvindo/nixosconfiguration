#
# Android Development
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    android-tools
    android-studio
    anbox
    waydroid
    apktool
    androguard
    adb-sync
  ];
}
