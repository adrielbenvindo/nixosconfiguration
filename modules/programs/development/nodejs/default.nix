#
# Nodejs Development
#

{ config, lib, pkgs, nix, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    nodejs
    pkgs.nodePackages_latest.npm
    pkgs.nodePackages_latest.pnpm
    yarn
    pkgs.nodePackages_latest.react-tools
    pkgs.nodePackages_latest.react-native-cli
    pkgs.nodePackages_latest.create-react-native-app
    pkgs.nodePackages_latest.expo-cli
    pkgs.nodePackages_latest.eas-cli
  ];
}
