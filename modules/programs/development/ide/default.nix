#
# IDE
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    eclipses.eclipse-java
  ];
}
