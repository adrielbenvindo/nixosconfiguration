#
# Dotnet Development
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    dotnet-sdk
    pkgs.dotnetPackages.Nuget
    nuget-to-nix
  ];
}
