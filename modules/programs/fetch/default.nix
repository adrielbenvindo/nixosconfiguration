#
# Fetch
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      neofetch
      ipfetch
    ];
  };
}
