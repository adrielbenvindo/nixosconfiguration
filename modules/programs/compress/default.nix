#
# Compress
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      xz
      zstd
      zip
      unzip
    ];
  };
}
