#
# Latex
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    texstudio
    lyx
  ];
}
