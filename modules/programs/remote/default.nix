#
# Remote
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      anydesk
      rustdesk
      termius
    ];
  };
}
