#
# Files
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    kompare
    krename
  ];
}
