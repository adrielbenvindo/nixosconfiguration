#
# Video
#

{ config, lib, pkgs, user, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      vlc
    ];
  };
  users.users.${user}.packages = with pkgs; [
    kaffeine
    pkgs.libsForQt5.plasmatube
  ];
}
