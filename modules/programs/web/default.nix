#
# Web
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      firefox
      opera
      vivaldi
      microsoft-edge
    ];
  };
}
