#
# Version Control
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      git
      mercurial
    ];
  };
    
}
