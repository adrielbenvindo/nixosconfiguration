#
# FTP Command line tools
#

{ config, lib, pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      filezilla
      taxi
    ];
  };
}
