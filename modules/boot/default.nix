{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ./grub.nix)] ++
    [(import ./efi.nix)] ++
    [(import ./grub_efi.nix)] ++
    #[(import ./systemd_boot.nix)] ++
    [(import ./ntfs.nix)];
}
