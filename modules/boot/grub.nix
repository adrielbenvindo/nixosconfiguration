#
# Grub configuration
#

{ config, pkgs, lib, ... }:

{
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.copyKernels = true;
  boot.loader.grub.fsIdentifier = "label";
  boot.loader.grub.splashImage = ../../resources/backgrounds/nixos-background.png;
  boot.loader.grub.splashMode = "stretch";
  
  boot.loader.grub.extraEntries = ''
   menuentry "Reboot" {
     reboot
   }
   menuentry "Poweroff" {
     halt
   }
  '';
}
