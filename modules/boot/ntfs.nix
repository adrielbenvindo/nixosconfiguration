#
# NTFS Configuration
#

{ config, pkgs, lib, ... }:

{
  boot.supportedFilesystems = [ "ntfs" ];
  #fileSystems."/path/to/mount/to" =
    #{ device = "/path/to/the/device";
      #fsType = "ntfs3"; 
      #options = [ "rw" "uid=theUidOfYourUser"];
    #};
}
