#
# Systemd-boot configuration
#

{ config, pkgs, lib, ... }:

{
  boot.loader.systemd-boot = {
    enable = true;
    configurationLimit = 5;
  };
}
