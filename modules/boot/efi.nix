#
# EFI Vars configuration
#

{ config, pkgs, lib, ... }:

{
  boot.loader.efi.canTouchEfiVariables = true;
}
