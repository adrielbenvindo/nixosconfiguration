#
# Grub efi configuration
#

{ config, pkgs, lib, ... }:

{
  #boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.devices = ["nodev"];
}
