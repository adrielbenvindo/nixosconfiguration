#
# Calc
#

{ config, lib, pkgs, user, ... }:

{
  users.users.${user}.packages = with pkgs; [
    qalculate-qt
  ];
}
