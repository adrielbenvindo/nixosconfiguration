{ lib, nixpkgs, inputs, home-manager, user, location, plasma-manager, ...}:#overlay-unstable nixpkgs-unstable

let
  system = "x86_64-linux";                                  # System architecture

  pkgs = import nixpkgs {
    inherit system;
    config.allowUnfree = true;                              # Allow proprietary software
    #config.allowBroken = true;
  };
in
{
  desktop = lib.nixosSystem{
    inherit system;
    specialArgs = {
      inherit user inputs location system;
      host = {
        hostName = "nixos-Adriel";
        #mainMonitor = "HDMI-A-3";
        #secondMonitor = "DP-1";
      };
    };
    modules = [
      #({ config, pkgs, ... }: { nixpkgs.overlays = [ overlay-unstable ]; })
      ./desktop
      ./configuration.nix
     
      home-manager.nixosModules.home-manager{
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.extraSpecialArgs = { 
          inherit user;
          host = {
            hostName = "nixos-Adriel";     #For Xorg iGPU  | Videocard 
            #mainMonitor = "HDMI-A-3"; #HDMIA3         | HDMI-A-1
            #secondMonitor = "DP-1";   #DP1            | DisplayPort-1
          };
        };
        home-manager.users.${user} = {
          imports = [
            ./home.nix 
            ./desktop/home.nix
            inputs.plasma-manager.homeManagerModules.plasma-manager
          ];
        };
      }
    ];
  };
}
