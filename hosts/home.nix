{ config, pkgs, lib, user, ...}:

{
        programs.home-manager.enable = true;
        
        home = {
          username = "${user}";
          homeDirectory = "/home/${user}";
          stateVersion = "22.11";
        };
        
	programs.git = {
		enable = true;
        	package = pkgs.gitAndTools.gitFull;
        	userName  = "Adriel de Oliveira Benvindo";
        	userEmail = "adrielbenvindo@outlook.com";
        	signing = {
                	key = "adrielbenvindo@outlook.com";
                	signByDefault = false;
        	};
	};
}
