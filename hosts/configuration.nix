# Edit this configuration file to define what should be installed on # your system. Help is available in the configuration.nix(5) man page 
# and in the NixOS manual (accessible by running ‘nixos-help’). 

{ config, lib, pkgs, inputs, user, ...}:#nixpkgs-unstable

{ 
  imports = [  
    ../bundles/desktop/essential
    #"${nixpkgs-unstable}/nixos/modules/services/x11/desktop-managers/plasma5.nix"
  ];

  #disabledModules = [
    #"services/x11/desktop-managers/plasma5.nix"
  #];
  
  networking.hostName = "nixos-Adriel"; # Define your hostname. 
  
  # Set your time zone. 
  time.timeZone = "America/Sao_Paulo"; 
  
  # Configure network proxy if necessary 
  # networking.proxy.default = "http://user:password@proxy:port/"; 
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain"; 
  
  # Select internationalisation properties. 
  i18n.defaultLocale = "en_US.UTF-8"; 
  console = { 
    font = "Lat2-Terminus16"; 
    keyMap = lib.mkDefault "br-abnt2"; 
    useXkbConfig = true; # use xkbOptions in tty. 
  }; 
  
  # Configure keymap in X11 
  services.xserver.layout = "br"; 
  # services.xserver.xkbOptions = { 
  # "eurosign:e"; 
  # "caps:escape" # map caps to escape. 
  # 
  # }; 
  
  # Enable CUPS to print documents. 
  # services.printing.enable = true; 
  
  #Insecure Packages 
  nixpkgs.config.permittedInsecurePackages = [ 
    #"libdwarf-20210528" 
    "qtwebkit-5.212.0-alpha4" 
  ]; 
  
  #Broken Packages 
  #nixpkgs.config.allowBroken = true; 
  
  #Unfree Packages 
  nixpkgs.config.allowUnfree = true; 
  
  #process eficiency
  security.rtkit.enable = true;
  security.polkit.enable = true;
  
  # Define a user account. Don't forget to set a password with ‘passwd’. 
  #users.defaultUserShell = pkgs.zsh;
  users.users.${user} = { 
    isNormalUser = true; 
    extraGroups = [ "wheel" "adbusers" "libvirtd" "networkmanager" "kvm"]; # Enable ‘sudo’ for the user. 
    shell = pkgs.nushell; 
    #packages = with pkgs; [ 
    #]; 
  }; 
  
  # List packages installed in system profile. To search, run: # $ nix search wget 
  environment = { 
    variables = { 
      TERMINAL = "konsole"; 
      EDITOR = "nano"; 
      VISUAL = "nano"; 
    }; 
    #systemPackages = with pkgs; [
    #];
  }; 
   
  system = { 
    #autoUpgrade = { 
      #enable = true; 
      #channel = "https://nixos.org/channels/nixos-unstable"; 
    #}; 
   
    stateVersion = "22.11"; 
    #copySystemConfiguration = true; 
  }; 
  
  # Some programs need SUID wrappers, can be configured further or are 
  # started in user sessions. 
  # programs.mtr.enable = true; 
  # programs.gnupg.agent = { 
  # enable = true; 
  # enableSSHSupport = true; 
  # }; 
  
  # List services that you want to enable: 
  # Enable the OpenSSH daemon. 
  # services.openssh.enable = true;
   
  # Open ports in the firewall. 
  #networking.firewall.allowedTCPPorts = [ 17500 ]; 
  #networking.firewall.allowedUDPPorts = [ 17500 ]; 
  # Or disable the firewall altogether. 
  # networking.firewall.enable = false; 
 
  #security for sudo 
  #security.sudo.wheelNeedsPassword = false; 
  
  nix = { 
    package = pkgs.nixVersions.unstable; 
    registry.nixpkgs.flake = inputs.nixpkgs;
    settings ={
      auto-optimise-store = true;           # Optimise syslinks
    };
    gc = { 
      automatic = true; 
      dates = "monthly"; 
      options = "--delete-older-than 1m"; 
    }; 
    extraOptions = '' 
      experimental-features = nix-command flakes 
      keep-outputs = true 
      keep-derivations = true 
    ''; 
  }; 
}
