{ pkgs, lib, user, ... }:

{
  imports =                                              
    [(import ./hardware-configuration.nix)] ++            
    [(import ../../bundles/essential)] ++       
    [(import ../../bundles/command_line)] ++
    [(import ../../bundles/desktop/apps)] ++
    [(import ../../bundles/desktop/auxiliar)] ++
    [(import ../../bundles/development)];
  
  boot = {
    #kernelPackages = pkgs.linuxKernel.packages.linux_hardened.kernel;
    loader.timeout = 5;
  };
  
  #hardware = {
  #};
  
  #environment = {                               # Packages installed system wide
    #systemPackages = with pkgs; [               # This is because some options need to be configured.
    #];
    #variables = {
      #LIBVA_DRIVER_NAME = "i965";
    #};
  #};
  
  #services = {
  #};
  
  #nixpkgs.overlays = [                          # This overlay will pull the latest version of Discord
    #(self: super: {
      #discord = super.discord.overrideAttrs (
        #_: { src = builtins.fetchTarball {
          #url = "https://discord.com/api/download?platform=linux&format=tar.gz";
          #sha256 = "1z980p3zmwmy29cdz2v8c36ywrybr7saw8n0w7wlb74m63zb9gpi";
        #};}
      #);
    #})
  #];                      
}
