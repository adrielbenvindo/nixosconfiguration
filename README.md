# NixOS / Nix Configuration



## About

This is my personal configuration files for NixOS with also configuration for nix, it was based on Mattias Beneat configuration [here](https://github.com/MatthiasBenaets/nixos-config)

## Structure

Programs and services are divided in modules and bundles to ensure flexibility

## Partitions

The partition table used on this are:

- /dev/sda1 : boot(500MB) fat32
- /dev/sda2 : swap(8GB) swap
- /dev/sda3 : nixos(400GB) btrfs
- /dev/sda4 : share(123GB) ntfs
- space for windows

## Sub-volumes

The subvolumes used on /dev/sda3 are:

- / : @
- /home : @home
- /nix : @nix
- /.snapshots : @.snapshots
- /var : @var
- /tmp : @tmp

## Usage

Anyone can use this configuration

make partitions as you want, but may have to copy /etc/nixos/hardware-configuration.nix to hosts/<host>/hardware-configuration.nix

to use the configuration the commands are:

```shell
nix-env -iA nixos.ntfs3g
nixos-generate-config --root /mnt
nix-env -iA nixos.git
git clone https://gitlab.com/adrielbenvindo/nixosconfiguration /mnt/etc/nixos/<name>
cd /mnt/etc/nixos/<name>
nixos-install --flake .#<host>
```