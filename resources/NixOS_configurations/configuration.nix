# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ...}:
let 
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-22.11.tar.gz"; 
  #plasma-manager = builtins.fetchGit{ url = "https://github.com/pjones/plasma-manager"; ref = "trunk"; };
  #nixgl = builtins.fetchTarball "archive/main.tar.gz";
  nixos = builtins.fetchGit{ url = "https://github/nixos/nixpkgs"; ref = "nixos-unstable"; };
  in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      #<home-manager/nixos>
      (import "${home-manager}/nixos")
      #(import "${plasma-manager}/flake.nix")
      #(import "${nixgl}")
      #(import "${nixos}/flake.nix")
    ];

  #boot.kernelPackages = pkgs.linuxKernel.packages.linux_hardened.kernel;
  boot.loader.timeout = 5;
  
  # Use the systemd-boot EFI boot loader.
  #boot.loader.systemd-boot.enable = true;
  #boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;
  
  #GRUB bootloader
  boot.loader.grub.enable = true;
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.copyKernels = true;
  #boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.grub.efiSupport = true;
  #boot.loader.grub.fsIdentifier = "boot";
  boot.loader.grub.fsIdentifier = "uuid";
  boot.loader.grub.splashImage = ./backgrounds/nixos-background.png;
  boot.loader.grub.splashMode = "stretch";

  boot.loader.grub.devices = ["nodev"];
  boot.loader.grub.extraEntries = ''
   menuentry "Reboot" {
     reboot
   }
   menuentry "Poweroff" {
     halt
   }
  '';
  
  #NTFS Support
  boot.supportedFilesystems = [ "ntfs" ];
  #fileSystems."/path/to/mount/to" =
    #{ device = "/path/to/the/device";
      #fsType = "ntfs3"; 
      #options = [ "rw" "uid=theUidOfYourUser"];
    #};

  #bluetooth
  hardware.bluetooth.enable = true;  

  networking.hostName = "nixos-Adriel"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "America/Sao_Paulo";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = lib.mkDefault "br-abnt2";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  
  #KDE
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  
  #services.xserver.desktopManager.plasma5.excludePackages = with pkgs.libsForQt5; [
  #];
  

  # Configure keymap in X11
  services.xserver.layout = "br";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  #hardware.pulseaudio.enable = true;
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
    jack.enable = true;
  };
  
  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true; 

  #mysql
  services.mysql.enable = true;
  services.mysql.package = pkgs.mysql80;  

  #libvirt
  virtualisation.libvirtd.enable = true;
  
  #docker
  virtualisation.docker.enable = true;

  #podman
  virtualisation.podman.enable = true;
  virtualisation.podman.defaultNetwork.dnsname.enable = true;
 
  #flatpak
  services.flatpak.enable = true;
  
  #dropbox
  systemd.user.services.dropbox = {
    description = "Dropbox";
    wantedBy = [ "graphical-session.target" ];
    environment = {
      QT_PLUGIN_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtPluginPrefix;
      QML2_IMPORT_PATH = "/run/current-system/sw/" + pkgs.qt5.qtbase.qtQmlPrefix;
    };
    serviceConfig = {
      ExecStart = "${pkgs.dropbox.out}/bin/dropbox";
      ExecReload = "${pkgs.coreutils.out}/bin/kill -HUP $MAINPID";
      KillMode = "control-group"; # upstream recommends process
      Restart = "on-failure";
      PrivateTmp = true;
      ProtectSystem = "full";
      Nice = 10;
    };
  };
  
  #onedrive
  services.onedrive.enable = true;
 
  #Insecure Packages
  nixpkgs.config.permittedInsecurePackages = [
      #"libdwarf-20210528"
      "qtwebkit-5.212.0-alpha4"
  ];

  #Broken Packages
  #nixpkgs.config.allowBroken = true;
  
  #Unfree Packages
  nixpkgs.config.allowUnfree = true;   
  
  #Android
  programs.adb.enable = true;
  
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.adrielbenvindo = {
    isNormalUser = true;
    extraGroups = [ "wheel" "adbusers" "libvirtd" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
    packages = with pkgs; [
      vscode
      texstudio
      lyx
      ghostwriter
      lapce
      nodejs
      pkgs.nodePackages_latest.npm
      pkgs.nodePackages_latest.pnpm
      yarn
      pkgs.nodePackages_latest.react-static
      pkgs.nodePackages_latest.react-tools
      pkgs.nodePackages_latest.react-native-cli      
      pkgs.nodePackages_latest.create-react-native-app
      pkgs.nodePackages_latest.expo-cli
      jdk
      jdk11
      spring-boot-cli
      rustup
      dotnet-sdk
      pkgs.dotnetPackages.Nuget
      nuget-to-nix
      pkgs.libsForQt5.kate
      krita
      gimp
      blender
      pkgs.libsForQt5.kolourpaint
      pkgs.libsForQt5.kcolorchooser
      pkgs.libsForQt5.kompare
      pkgs.libsForQt5.korganizer
      pkgs.libsForQt5.kaddressbook
      pkgs.libsForQt5.kapptemplate
      #okteta
      ktimer
      kronometer
      #elf-dissector
      pkgs.libsForQt5.calindori
      pkgs.libsForQt5.plasmatube
      pkgs.libsForQt5.keysmith
      ktimetracker
      kaffeine
      #kexi
      krename
      pkgs.libsForQt5.zanshin
      pkgs.libsForQt5.kalarm
      pkgs.libsForQt5.marble
      pkgs.libsForQt5.umbrello
      staruml
      pkgs.libsForQt5.kmail
      pkgs.libsForQt5.kalzium
      pkgs.libsForQt5.kruler
      gitkraken
      github-desktop
      android-tools
      android-studio
      anbox
      waydroid
      apktool
      androguard
      adb-sync
      mailspring
      thunderbird
      simplenote
      qalculate-qt
      appimagekit
      #ciscoPacketTracer8
      #ciscoPacketTracer7
      #dropbox
      dropbox-cli
      micro
      #mysql-workbench
      mysql-shell
      dbeaver
      figma-linux
      eclipses.eclipse-java
      pcloud
    ];
  };

  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment = {
    variables = {
      EDITOR = "nano";
      TERMINAL = "konsole";
      VISUAL = "nano";
    };
    systemPackages = with pkgs; [
      niv
      simplescreenrecorder
      vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by defa>
      neovim
      wget
      curlFull
      firefox
      opera
      vivaldi
      #docker
      docker-compose
      docker-machine
      docker-sync
      docker-ls
      #podman
      podman-compose
      pods
      podman-tui
      distrobox
      ventoy-bin-full
      xz
      zstd
      zip
      #flatpak
      libsForQt5.ark
      htop
      neofetch
      ipfetch
      pkgs.libsForQt5.falkon
      pkgs.libsForQt5.konqueror
      pkgs.libsForQt5.knotes
      pkgs.libsForQt5.kdeconnect-kde
      pkgs.libsForQt5.yakuake
      pkgs.libsForQt5.kcalc
      pkgs.libsForQt5.kfind
      pkgs.libsForQt5.discover
      pkgs.libsForQt5.kalendar
      pkgs.libsForQt5.ktorrent
      pkgs.libsForQt5.kget
      pkgs.libsForQt5.filelight
      partition-manager
      pkgs.libsForQt5.kmag
      digikam
      pkgs.libsForQt5.kmix
      pkgs.libsForQt5.krfb
      pkgs.libsForQt5.krdc
      pkgs.libsForQt5.kamoso
      pkgs.libsForQt5.kgpg
      git
      mercurial
      #ntfs3g
      #zsh
      ksh
      anydesk
      rustdesk
      bleachbit
      vlc
      qemu_full
      #libvirt
      virt-manager-qt
      libreoffice-fresh
      starship
    ];
  };
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 19000 ]; # 17500 ];
  networking.firewall.allowedUDPPorts = [ 19000 ]; # 17500 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  
  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

  programs.zsh = {
        enable = true;
	enableBashCompletion = true;
        zsh-autoenv.enable = true;
        syntaxHighlighting.enable = true;
        autosuggestions.enable = true;
                
        shellAliases = {
                ll = "ls -l";
                update = "sudo nixos-rebuild switch";
        };
        shellInit = "export ANDROID_HOME=$HOME/Android/Sdk \n export PATH=$PATH:$ANDROID_HOME/emulator \n export PATH=$PATH:$ANDROID_HOME/platform-tools";
        histSize = 10000;
        histFile = "$HOME/.zsh_history";
  };

  programs.starship = {
    enable = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
      # add_newline = false;

      # character = {
      #   success_symbol = "[➜](bold green)";
      #   error_symbol = "[➜](bold red)";
      # };

      # package.disabled = true;
    };
  };

  nix = {
    #package = pkgs.nixFlakes;
    #extraOptions = "experimental-features = nix-command flakes";
    settings.experimental-features = [ "nix-command" "flakes" ];
  };
  
  
  #Home manager
  #home-manager.enable = true;
  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;
  home-manager.extraSpecialArgs = { 
    host = {
      hostName = "nixos-Adriel";     #For Xorg iGPU  | Videocard 
      #mainMonitor = "HDMI-A-3"; #HDMIA3         | HDMI-A-1
      #secondMonitor = "DP-1";   #DP1            | DisplayPort-1
    };
  };
  home-manager.users.adrielbenvindo = {
        home.stateVersion = "22.11";
      	programs.git = {
        	enable = true;
        	package = pkgs.gitAndTools.gitFull;
        	userName  = "Adriel de Oliveira Benvindo";
        	userEmail = "adrielbenvindo@outlook.com";
        	signing = {
                	key = "adrielbenvindo@outlook.com";
                	signByDefault = false;
        	};
	};
  };
}
