{ config, pkgs, lib, user, ... }:

{
  imports = 
    [(import ../../modules/programs/development/android)] ++
    [(import ../../modules/programs/development/auxiliar)] ++
    [(import ../../modules/programs/development/database_management)] ++
    [(import ../../modules/programs/development/dotnet)] ++
    [(import ../../modules/programs/development/editor)] ++
    [(import ../../modules/programs/development/git)] ++
    [(import ../../modules/programs/development/ide)] ++
    [(import ../../modules/programs/development/java)] ++
    [(import ../../modules/programs/development/modeling)] ++
    [(import ../../modules/programs/development/nodejs)] ++
    [(import ../../modules/programs/development/prototyping)] ++
    [(import ../../modules/programs/development/testing)] ++
    [(import ../../modules/programs/development/rust)] ++
    [(import ../../modules/programs/development/python)] ++
    [(import ../../modules/programs/development/lsp_servers)];
    #[(import ../../modules/containers)];
}   
