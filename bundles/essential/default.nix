{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ../../modules/boot)] ++
    [(import ../../modules/hardware)] ++
    [(import ../../modules/network)] ++
    [(import ../../modules/os_management)] ++
    [(import ../../modules/filesystem)] ++
    [(import ../../modules/security)];
}
