{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ../../modules/command_line_editors)] ++
    [(import ../../modules/command_line_tools)] ++
    [(import ../../modules/shell)];
}
