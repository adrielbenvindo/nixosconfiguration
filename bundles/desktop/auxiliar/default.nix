{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ../../../modules/desktop/cloud)] ++
    [(import ../../../modules/desktop/virtualisation)] ++
    [(import ../../../modules/calc)] ++
    [(import ../../../modules/notes)] ++
    [(import ../../../modules/social)] ++
    [(import ../../../modules/installation)] ++
    [(import ../../../modules/services)] ++
    [(import ../../../modules/acess)];
}
