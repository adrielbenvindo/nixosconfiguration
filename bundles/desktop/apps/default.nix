{ pkgs, lib, user, ... }:

{
  imports = 
    [(import ../../../modules/programs/audio)] ++
    [(import ../../../modules/programs/calendar)] ++
    [(import ../../../modules/programs/compress)] ++
    [(import ../../../modules/programs/data)] ++
    [(import ../../../modules/programs/email)] ++
    [(import ../../../modules/programs/fetch)] ++
    [(import ../../../modules/programs/files)] ++
    [(import ../../../modules/programs/graphical)] ++
    [(import ../../../modules/programs/latex)] ++
    [(import ../../../modules/programs/markdown)] ++
    [(import ../../../modules/programs/office)] ++
    [(import ../../../modules/programs/remote)] ++
    [(import ../../../modules/programs/screen)] ++
    [(import ../../../modules/programs/security)] ++
    [(import ../../../modules/programs/tasks)] ++
    [(import ../../../modules/programs/time)] ++
    [(import ../../../modules/programs/version_control)] ++
    [(import ../../../modules/programs/video)] ++
    [(import ../../../modules/programs/web)] ++
    [(import ../../../modules/programs/terminal)];
}   
