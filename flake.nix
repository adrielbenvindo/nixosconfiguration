{
  description = "My personal configuration with flakes";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    #nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = github:nix-community/home-manager/release-22.11;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixgl = {                                                             # OpenGL
      url = "github:guibou/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    plasma-manager = {                                                    # KDE Plasma user settings
      url = "github:pjones/plasma-manager";                               # Add "inputs.plasma-manager.homeManagerModules.plasma-manager" to the home-manager.users.${user}.imports
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "nixpkgs";
    };
  };

  outputs = inputs @ { self, nixpkgs, home-manager, nixgl, plasma-manager, ... }:#nixpkgs-unstable
    let 
      user = "adrielbenvindo";
      location = "$HOME/.system_configuration";
      #overlay-unstable = final: prev: {
        #unstable = nixpkgs-unstable;
      #};
    in{
      nixosConfigurations = ( import ./hosts ) {
        inherit (nixpkgs) lib;
        inherit inputs user nixpkgs home-manager location plasma-manager;#overlay-unstable nixpkgs-unstable
      };
      
      homeConfigurations = ( import ./nix ) {        #Non-Nixos Configuration
        inherit (nixpkgs) lib;
        inherit inputs nixpkgs home-manager nixgl user;
      };
    };
    
}
